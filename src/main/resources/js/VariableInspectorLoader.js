
$ = AJS.$;

(function ($, BAMBOO) {
    BAMBOO.VariableInspectorLoader = {};

    /**
     * Load all the available bamboo variables..
     *
     * @param {string} planKey The key of the plan to get info for.
     * @return {jQuery.Deferred} A deferred that is resolved with the variable information and the JSON response.
     */
    function loadVariablesAvailableInPlan(planKey) {
        var deferred = jQuery.Deferred(),
            request;

        request = jQuery.ajax({
            contentType: "application/json",
            dataType: "json",
            url: AJS.contextPath() + "/rest/variables/1.0/variables/" + planKey,
        });

        request.error(function () {
            deferred.reject();
        });

        request.success(function (response) {
            deferred.resolve(response);
        });

        return deferred.promise();
    };

    /**
     * Resolve variable information that is contained in a string.
     * @param variableData The variable data to resolve.
     * @param planKey The plan key to look up information in.
     * @returns {jQuery.Deferred} A deferred that is resolved with the actual information in the bamboo variables.
     */
    function resolveVariableInformation(variableData, planKey) {
        var deferred = jQuery.Deferred(),
            reqyest;

        request = jQuery.ajax({
            contentType: "application/x-www-form-urlencoded",
            dataType: "json",
            type: 'POST',
            url: AJS.contextPath() + "/rest/variables/1.0/variables/resolve",
            data:"planAndJobKey=" + planKey + "&keys=" + variableData
        });

        request.error(function () {
            deferred.reject();
        });

        request.success(function (response) {
            deferred.resolve(response);
        });

        return deferred.promise();
    };

    BAMBOO.VariableInspectorLoader.loadVariablesAvailableInPlan = loadVariablesAvailableInPlan;
    BAMBOO.VariableInspectorLoader.resolveVariableInformation = resolveVariableInformation;


}(jQuery, window.BAMBOO = (window.BAMBOO || {})));