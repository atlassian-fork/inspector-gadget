package com.atlassian.mhenderson.bamboo.plugins.inspectorgadget.jobs;

import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plan.cache.CachedPlanManager;
import com.atlassian.bamboo.plan.cache.ImmutablePlan;

import java.util.Map;

/**
 * Defines the operations on the plan retrieving scheduler.
 * Not currently used at the moment.
 */
public interface PlanRetrievingScheduler
{
    String SCHEDULER_KEY = PlanRetrievingScheduler.class.getName() + ":instance";
    String PLAN_MANAGER_KEY = PlanManager.class.getName() + ":instance";
    String CACHED_PLAN_MANAGER_KEY = CachedPlanManager.class.getName() + ":instance";

    /**
     * Set the map of plan vs build definitions.
     * @param map
     */
    void setMap(Map<ImmutablePlan, BuildDefinition> map);
}
