package com.atlassian.mhenderson.bamboo.plugins.inspectorgadget.services;

import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.util.PasswordMaskingUtils;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.atlassian.bamboo.variable.CustomVariableContext;
import com.atlassian.bamboo.variable.VariableDefinition;
import com.atlassian.bamboo.variable.VariableDefinitionContext;
import com.atlassian.bamboo.variable.VariableDefinitionManager;
import com.atlassian.bamboo.variable.substitutor.VariableSubstitutor;
import com.atlassian.mhenderson.bamboo.plugins.inspectorgadget.rest.BambooVariable;
import com.atlassian.mhenderson.bamboo.plugins.inspectorgadget.rest.BambooVariablesResource;
import com.google.common.collect.ImmutableList;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Provides methods for looking up bamboo variables by name, id, getting all variables
 * for a plan etc.
 */
public class BambooVariableSearchServiceImpl implements BambooVariableSearchService {

    private static final Logger log = Logger.getLogger(BambooVariablesResource.class);

    private static final String OBSCURER = "*";

    private final VariableDefinitionManager variableDefinitionManager;
    private final PlanManager planManager;
    private final CustomVariableContext customVariableContext;

    /**
     * Constructor.
     * @param variableDefinitionManager
     * @param planManager
     * @param customVariableContext
     */
    public BambooVariableSearchServiceImpl(final VariableDefinitionManager variableDefinitionManager,
                                           final PlanManager planManager,
                                           final CustomVariableContext customVariableContext) {
        this.variableDefinitionManager = variableDefinitionManager;
        this.planManager = planManager;
        this.customVariableContext = customVariableContext;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public VariableDefinition lookupGlobalVariableByName(final String variableKey) {
        List<VariableDefinition> globalDefinitions = getGlobalVariables();
        return findVariableByKey(variableKey, globalDefinitions);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public VariableDefinition lookupPlanVariableByName(final String planAndJobKey, final String variableKey) {
        List<VariableDefinition> planDefinitions = getPlanVariablesSafely(planAndJobKey);
        return findVariableByKey(variableKey, planDefinitions);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public VariableDefinition lookupVariableById(final String variableId) {
        return variableDefinitionManager.findVariableDefinition(new Long(variableId));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<BambooVariable> prepareFullListOfVariables(List<VariableDefinition> variableDefinitions) {
        List<BambooVariable> variables = new ArrayList<BambooVariable>();
        for (VariableDefinition definition : variableDefinitions) {
            BambooVariable variable = new BambooVariable(definition.getKey(),
                    obscurePasswordForKeyIfNecessary(definition.getKey(), definition.getValue()), definition.getVariableType().toString());
            variables.add(variable);
        }

        return variables;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<VariableDefinition> getAllVariableDefinitions(String planAndJobKey) {
        List<VariableDefinition> planDefinitions = getPlanVariablesSafely(planAndJobKey);
        List<VariableDefinition> globalDefinitions = getGlobalVariables();
        return new ImmutableList.Builder<VariableDefinition>()
                .addAll(planDefinitions).addAll(globalDefinitions).build();
    }

    @Override
    public List<VariableDefinition> getGlobalVariables() {
        List<VariableDefinition> globalDefinitions = variableDefinitionManager.getGlobalVariables();
        logDebug("getGlobalVariables - found:" + globalDefinitions.size() + " global variables.");
        return globalDefinitions;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<VariableDefinition> getPlanVariablesSafely(String planAndJobKey) {
        List<VariableDefinition> planDefinitions = new ArrayList<VariableDefinition>();
        try {
            PlanKey currentPlanKey = extractPlanKeyFromPlanAndJobKey(planAndJobKey);
            Plan currentPlan = planManager.getPlanByKey(currentPlanKey);
            planDefinitions = variableDefinitionManager.getPlanVariables(currentPlan);
        } catch (Exception e) {
            log.error("An exception occurred retrieving plan variables:" + e.getMessage());
        }

        logDebug("getPlanVariablesSafely - found:" + planDefinitions.size() + " for key:" + planAndJobKey);
        return planDefinitions;
    }

    /**
     * {@inheritDoc}
     * @todo This needs implementing.
     */
    @Override
    public void getPreviousBuildRuntimeVariables(String planAndJobKey) {
        BuildContext previousBuildContext = null;
        VariableSubstitutor variableSubstitutor = customVariableContext.getVariableSubstitutorFactory().newSubstitutorForCommonContext(previousBuildContext);
        Map<String, VariableDefinitionContext> runtimeVariables = variableSubstitutor.getVariableContexts();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public VariableDefinition findVariableByKey(String key, List<VariableDefinition> definitions){
        VariableDefinition result = null;
        for (VariableDefinition definition: definitions) {
            if (definition.getKey().equals(key)) {
                result = definition;
                break;
            }
        }

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String obscurePasswordForKeyIfNecessary(String key, String value) {
        return PasswordMaskingUtils.shouldBeMasked(key) ? PasswordMaskingUtils.PASSWORD_MASK : value;
    }

    /**
     * {@inheritDoc}
     */
    private PlanKey extractPlanKeyFromPlanAndJobKey(String planAndJobKey) {
        PlanKey fullKey = PlanKeys.getPlanKey(planAndJobKey);
        if (PlanKeys.isJobKey(fullKey)) {
            PlanKey key = PlanKeys.getPlanKey(planAndJobKey);
            return PlanKeys.getChainKeyFromJobKey(key);
        } else {
            return fullKey;
        }
    }

    /**
     * {@inheritDoc}
     */
    private void logDebug(String message) {
        if (log.isDebugEnabled()) {
            log.debug(message);
        }
    }
}
