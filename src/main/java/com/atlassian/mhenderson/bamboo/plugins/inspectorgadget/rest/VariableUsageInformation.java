package com.atlassian.mhenderson.bamboo.plugins.inspectorgadget.rest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * A class that contains all the of the places in which a variable is actually used in plans.
 */
@XmlRootElement(name = "bamboo-variable-usage")
@XmlAccessorType(XmlAccessType.FIELD)
public class VariableUsageInformation {

    private String variableId;
    private String variableKey;
    private String variableValue;
    private List<VariableUsageInPlanInformation> planUsageLocations = new ArrayList<VariableUsageInPlanInformation>();
    private Collection<VariableUsageInRepositoryInformation> repositoryUsageLocations = new HashSet<VariableUsageInRepositoryInformation>();

    public VariableUsageInformation(){}

    public VariableUsageInformation(final String variableId, final String variableKey, final String variableValue) {
        this.variableId = variableId;
        this.variableKey = variableKey;
        this.variableValue = variableValue;
    }

    public String getVariableId() {
        return variableId;
    }

    public void setVariableId(final String variableId) {
        this.variableId = variableId;
    }

    public String getVariableKey() {
        return variableKey;
    }

    public void setVariableKey(final String variableKey) {
        this.variableKey = variableKey;
    }

    public String getVariableValue() {
        return variableValue;
    }

    public void setVariableValue(final String variableValue) {
        this.variableValue = variableValue;
    }

    public List<VariableUsageInPlanInformation> getVariableUsageLocations() {
        return planUsageLocations;
    }

    public void setVariableUsageLocations(final List<VariableUsageInPlanInformation> variableUsageLocations) {
        this.planUsageLocations = variableUsageLocations;
    }

    public void addVariableUsageLocation(VariableUsageInPlanInformation location) {
        this.planUsageLocations.add(location);
    }

    public Collection<VariableUsageInRepositoryInformation> getVariableUsageInRepositoryLocations() {
        return repositoryUsageLocations;
    }

    public void setVariableUsageInRepositoryLocations(final Collection<VariableUsageInRepositoryInformation> variableUsageLocations) {
        this.repositoryUsageLocations = variableUsageLocations;
    }

    public void addVariableUsageInRespositoryLocation(VariableUsageInRepositoryInformation location) {
        this.repositoryUsageLocations.add(location);
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        final VariableUsageInformation that = (VariableUsageInformation) o;

        if (planUsageLocations != null ? !planUsageLocations.equals(that.planUsageLocations) : that.planUsageLocations != null)
        {
            return false;
        }
        if (repositoryUsageLocations != null ? !repositoryUsageLocations.equals(that.repositoryUsageLocations) : that.repositoryUsageLocations != null)
        {
            return false;
        }
        if (variableId != null ? !variableId.equals(that.variableId) : that.variableId != null)
        {
            return false;
        }
        if (variableKey != null ? !variableKey.equals(that.variableKey) : that.variableKey != null)
        {
            return false;
        }
        if (variableValue != null ? !variableValue.equals(that.variableValue) : that.variableValue != null)
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = variableId != null ? variableId.hashCode() : 0;
        result = 31 * result + (variableKey != null ? variableKey.hashCode() : 0);
        result = 31 * result + (variableValue != null ? variableValue.hashCode() : 0);
        result = 31 * result + (planUsageLocations != null ? planUsageLocations.hashCode() : 0);
        result = 31 * result + (repositoryUsageLocations != null ? repositoryUsageLocations.hashCode() : 0);
        return result;
    }
}
