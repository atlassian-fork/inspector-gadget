package com.atlassian.mhenderson.bamboo.plugins.inspectorgadget.services;

import com.atlassian.bamboo.plan.cache.ImmutablePlan;
import com.atlassian.mhenderson.bamboo.plugins.inspectorgadget.rest.VariableUsageInRepositoryInformation;

import java.util.List;

/**
 * Defines the operation for searching repositories for variable usages.
 */
public interface BambooRepositorySearchService
{
    /**
     * Find usages of variable in shared repositories.
     * @param variableKey
     * @return List<VariableUsageInRepositoryInformation> The list of repositories the variable key is used in
     * or an empty list if not found in any.
     */
    List<VariableUsageInRepositoryInformation> findUsagesOfVariable(String variableKey);

    /**
     * Finds all usages of a variable in all of the repositories for all plans, if found then provide information
     * as to which plan it is used in.
     * @param variableKey
     * @return List
     * @throws Exception
     */
    List<VariableUsageInRepositoryInformation> findUsagesOfVariableInPlanRepositories(final String variableKey);

    /**
     * Finds all usages of a variable in all of the repositories for a plan, if found then provide information
     * as to which plan it is used in.
     * @param variableKey
     * @return List
     * @throws Exception
     */
    List<VariableUsageInRepositoryInformation> findUsagesOfVariable(final String variableKey, ImmutablePlan plan);
}
