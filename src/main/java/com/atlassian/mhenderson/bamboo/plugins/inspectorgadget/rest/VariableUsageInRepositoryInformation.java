package com.atlassian.mhenderson.bamboo.plugins.inspectorgadget.rest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * A class that contains all the of the places in which a variable is actually used in repositories.
 */
@XmlRootElement(name = "bamboo-variable-usage-repository")
@XmlAccessorType(XmlAccessType.FIELD)
public class VariableUsageInRepositoryInformation
{
    private String repositoryName;
    private long repositoryId;
    private boolean global;

    private List<String> planKeys = new ArrayList<String>();

    public VariableUsageInRepositoryInformation(){}

    public VariableUsageInRepositoryInformation(final long repositoryId, final String repositoryName, final boolean isGlobal, final List<String> planKeys) {
        this.repositoryId = repositoryId;
        this.repositoryName = repositoryName;
        this.global = isGlobal;
        this.planKeys = planKeys;
    }

    public long getRepositoryId() {
        return repositoryId;
    }

    public void setRepositoryId(final long repositoryId) {
        this.repositoryId = repositoryId;
    }

    public String getRepositoryName() {
        return repositoryName;
    }

    public void setRepositoryName(final String repositoryName) {
        this.repositoryName = repositoryName;
    }

    public boolean isGlobal() {
        return global;
    }

    public void setGlobal(final boolean global) {
        this.global = global;
    }

    public List<String> getPlanKeys() {
        return planKeys;
    }

    public void setPlanKeys(final List<String> planKeys) {
        this.planKeys = planKeys;
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final VariableUsageInRepositoryInformation that = (VariableUsageInRepositoryInformation) o;
        if (global != that.global) {
            return false;
        }

        if (repositoryId != that.repositoryId) {
            return false;
        }

        if (planKeys != null ? !planKeys.equals(that.planKeys) : that.planKeys != null) {
            return false;
        }

        if (repositoryName != null ? !repositoryName.equals(that.repositoryName) : that.repositoryName != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = repositoryName != null ? repositoryName.hashCode() : 0;
        result = 31 * result + (int) (repositoryId ^ (repositoryId >>> 32));
        result = 31 * result + (global ? 1 : 0);
        result = 31 * result + (planKeys != null ? planKeys.hashCode() : 0);
        return result;
    }
}
